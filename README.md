Find a dog- app

This app is simple tool for finding more information about certain dog breeds.
This app is still in development so the number of breeds in the app is not huge.

When the app is opened a list of different breeds is displayed. Under the name of the breed a 
FCI group and the description of the group can be found. 
By clicking the name of the breed a picture of the breed opens up. Under the picture 
there is a button which will lead you to a breeds wikipedia page for more information.
Please watch the video provided before use in order to understand the app better.

Thank you!

Niina Taipale 0452693
